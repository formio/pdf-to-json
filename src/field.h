#ifndef FIELD_H
#define FIELD_H

#include <QtCore>
#include <optional>
#include "poppler-qt5.h"
#include "poppler-form.h"

namespace PdfToJson {
    class Field : public Poppler::FormField{
    public:
        QJsonArray serializeRect(QRectF);
        QJsonArray serializeList(QStringList);
        template <class T>
        QJsonArray serializeList(QList<T>);
        QString getType();
        QJsonObject *serializeToJson();
    };
}

#endif
