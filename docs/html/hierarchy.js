var hierarchy =
[
    [ "Poppler::Document", null, [
      [ "PdfToJson::Document", "class_pdf_to_json_1_1_document.html", null ]
    ] ],
    [ "Poppler::FormField", null, [
      [ "PdfToJson::Field", "class_pdf_to_json_1_1_field.html", [
        [ "PdfToJson::ButtonField", "class_pdf_to_json_1_1_button_field.html", null ],
        [ "PdfToJson::ChoiceField", "class_pdf_to_json_1_1_choice_field.html", null ],
        [ "PdfToJson::TextField", "class_pdf_to_json_1_1_text_field.html", null ]
      ] ]
    ] ],
    [ "Poppler::FormFieldButton", null, [
      [ "PdfToJson::ButtonField", "class_pdf_to_json_1_1_button_field.html", null ]
    ] ],
    [ "Poppler::FormFieldChoice", null, [
      [ "PdfToJson::ChoiceField", "class_pdf_to_json_1_1_choice_field.html", null ]
    ] ],
    [ "Poppler::FormFieldText", null, [
      [ "PdfToJson::TextField", "class_pdf_to_json_1_1_text_field.html", null ]
    ] ],
    [ "Poppler::Page", null, [
      [ "PdfToJson::Page", "class_pdf_to_json_1_1_page.html", null ]
    ] ]
];