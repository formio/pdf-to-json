var files_dup =
[
    [ "button-field.cpp", "button-field_8cpp.html", null ],
    [ "button-field.h", "button-field_8h.html", [
      [ "ButtonField", "class_pdf_to_json_1_1_button_field.html", "class_pdf_to_json_1_1_button_field" ]
    ] ],
    [ "choice-field.cpp", "choice-field_8cpp.html", null ],
    [ "choice-field.h", "choice-field_8h.html", [
      [ "ChoiceField", "class_pdf_to_json_1_1_choice_field.html", "class_pdf_to_json_1_1_choice_field" ]
    ] ],
    [ "document.cpp", "document_8cpp.html", null ],
    [ "document.h", "document_8h.html", [
      [ "Document", "class_pdf_to_json_1_1_document.html", "class_pdf_to_json_1_1_document" ]
    ] ],
    [ "field.cpp", "field_8cpp.html", null ],
    [ "field.h", "field_8h.html", [
      [ "Field", "class_pdf_to_json_1_1_field.html", "class_pdf_to_json_1_1_field" ]
    ] ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "page.cpp", "page_8cpp.html", null ],
    [ "page.h", "page_8h.html", [
      [ "Page", "class_pdf_to_json_1_1_page.html", "class_pdf_to_json_1_1_page" ]
    ] ],
    [ "text-field.cpp", "text-field_8cpp.html", null ],
    [ "text-field.h", "text-field_8h.html", [
      [ "TextField", "class_pdf_to_json_1_1_text_field.html", null ]
    ] ]
];